/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import dao.ConnectionFactory;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author carlos
 */
public class Relatorio {
    
    Connection conn;
    String local = "";
    public Relatorio(){
        local = System.getProperty("user.dir") + "/src/report/";
    }
    public void saidaUrbana( int codigo ){
        conn = ConnectionFactory.getConnection();        
        String localFile = local + "saidaUrbana.jrxml";
        try{
           conn = ConnectionFactory.getConnection();
           
           JasperDesign design = JRXmlLoader.load( localFile );
           
           JasperReport jr = JasperCompileManager.compileReport(design);
           
           HashMap valores = new HashMap();
           valores.put("id",codigo);
           
           JasperPrint impressao = JasperFillManager.fillReport(jr,valores,conn);
           
           JasperViewer jrViewer = new JasperViewer(impressao, false);
           
           jrViewer.setVisible(true);
           jrViewer.setDefaultCloseOperation(JasperViewer.DISPOSE_ON_CLOSE);
           
           
       }
       catch(JRException e){
           JOptionPane.showMessageDialog(null, "Erro no relatorio");
           System.err.println(e.getMessage());
                   
       }
    }
    
    public void saidaRural( int codigo ){
        conn = ConnectionFactory.getConnection();
        System.out.println("Url: "+local + "saidaRural.jrxml");
        String localFile = local + "saidaRural.jrxml";
        try{
           conn = ConnectionFactory.getConnection();
           
           JasperDesign design = JRXmlLoader.load( localFile );
           
           JasperReport jr = JasperCompileManager.compileReport(design);
           
           HashMap valores = new HashMap();
           valores.put("id",codigo);
           
           JasperPrint impressao = JasperFillManager.fillReport(jr,valores,conn);
           
           JasperViewer jrViewer = new JasperViewer(impressao, false);
           
           jrViewer.setVisible(true);
           jrViewer.setDefaultCloseOperation(JasperViewer.DISPOSE_ON_CLOSE);
           
           
       }
       catch(JRException e){
           JOptionPane.showMessageDialog(null, "Erro no relatorio");
           System.err.println(e.getMessage());
                   
       }
    }
}
