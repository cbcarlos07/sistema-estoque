-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema estoque
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema estoque
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `estoque` DEFAULT CHARACTER SET utf8 ;
USE `estoque` ;

-- -----------------------------------------------------
-- Table `estoque`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`usuario` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NULL,
  `login` VARCHAR(45) NULL,
  `senha` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`bairro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`bairro` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`zona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`zona` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`comunidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`comunidade` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NULL,
  `observacao` VARCHAR(255) NULL,
  `nome_lider` VARCHAR(150) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`cargo` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`pessoa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`pessoa` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `cargo` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pessoa_cargo1_idx` (`cargo` ASC),
  CONSTRAINT `fk_pessoa_cargo1`
    FOREIGN KEY (`cargo`)
    REFERENCES `estoque`.`cargo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`area`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`area` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NULL,
  `observacao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`escola`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`escola` (
  `id` INT ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `diretor` INT UNSIGNED NOT NULL,
  `zona` INT UNSIGNED NOT NULL,
  `rua` VARCHAR(150) NULL,
  `comunidade` INT NULL,
  `bairro` INT NULL,
  `qt_alunos` INT NULL,
  `area` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_escola_pessoa1_idx` (`diretor` ASC),
  INDEX `fk_escola_zona1_idx` (`zona` ASC),
  INDEX `fk_escola_area1_idx` (`area` ASC),
  CONSTRAINT `fk_escola_pessoa1`
    FOREIGN KEY (`diretor`)
    REFERENCES `estoque`.`pessoa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_escola_zona1`
    FOREIGN KEY (`zona`)
    REFERENCES `estoque`.`zona` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_escola_area1`
    FOREIGN KEY (`area`)
    REFERENCES `estoque`.`area` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`unidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`unidade` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(100) NULL,
  `sigla` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`produto` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NULL,
  `unidade` INT UNSIGNED NOT NULL,
  `qtde` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_produto_unidade1_idx` (`unidade` ASC),
  CONSTRAINT `fk_produto_unidade1`
    FOREIGN KEY (`unidade`)
    REFERENCES `estoque`.`unidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`fornecedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`fornecedor` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`entrada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`entrada` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `produto` INT UNSIGNED NOT NULL,
  `qtde` INT NULL,
  `dt_entrada` DATE NULL,
  `fornecedor` INT NOT NULL,
  `preco_unit` DECIMAL(10,2) NULL,
  `usuario` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_entrada_produto1_idx` (`produto` ASC),
  INDEX `fk_entrada_fornecedor1_idx` (`fornecedor` ASC),
  INDEX `fk_entrada_usuario1_idx` (`usuario` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `estoque`.`saida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`saida` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `destino` INT ZEROFILL UNSIGNED NOT NULL,
  `dt_saida` DATE NULL,
  `pessoa_recebeu` VARCHAR(255) NULL,
  `sn_recebido` CHAR(1) NULL DEFAULT 'N',
  `dt_recebido` DATE NULL,
  `usuario` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_saida_escola1_idx` (`destino` ASC),
  INDEX `fk_saida_usuario1_idx` (`usuario` ASC),
  CONSTRAINT `fk_saida_escola1`
    FOREIGN KEY (`destino`)
    REFERENCES `estoque`.`escola` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_saida_usuario1`
    FOREIGN KEY (`usuario`)
    REFERENCES `estoque`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estoque`.`saidaproduto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estoque`.`saidaproduto` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `saida` INT UNSIGNED NOT NULL,
  `produto` INT UNSIGNED NOT NULL,
  `qtde` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_table1_produto1_idx` (`produto` ASC),
  INDEX `fk_table1_saida1_idx` (`saida` ASC),
  CONSTRAINT `fk_table1_produto1`
    FOREIGN KEY (`produto`)
    REFERENCES `estoque`.`produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_saida1`
    FOREIGN KEY (`saida`)
    REFERENCES `estoque`.`saida` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `estoque`.`usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `estoque`;
INSERT INTO `estoque`.`usuario` (`id`, `nome`, `login`, `senha`) VALUES (DEFAULT, 'Administrador', 'admin', MD5('12345678'));

COMMIT;

USE `estoque`;

DELIMITER $$
USE `estoque`$$
CREATE DEFINER = CURRENT_USER TRIGGER `estoque`.`entrada_AFTER_INSERT` AFTER INSERT ON `entrada` FOR EACH ROW
BEGIN
   SET @v_qtde = ( SELECT qtde FROM produto WHERE id = NEW.produto );
   SET @v_total = @v_qtde + NEW.qtde;
   UPDATE produto SET qtde = @v_total WHERE id = NEW.produto;
END$$

USE `estoque`$$
CREATE DEFINER = CURRENT_USER TRIGGER `estoque`.`saidaproduto_AFTER_INSERT` AFTER INSERT ON `saidaproduto` FOR EACH ROW
BEGIN
  SET @v_qtde = ( SELECT qtde FROM produto WHERE id = NEW.produto );
  SET @v_total = @v_qtde - NEW.qtde;
  UPDATE produto SET qtde = @v_total WHERE id = NEW.produto;
END$$


DELIMITER ;
